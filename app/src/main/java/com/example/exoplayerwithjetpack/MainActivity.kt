package com.example.exoplayerwithjetpack

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import com.example.exoplayerwithjetpack.ui.theme.ExoPlayerWithJetpackTheme
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ExoPlayerWithJetpackTheme {
                ExoPlayer()
            }
        }
    }
}

@Composable
fun ExoPlayer() {
    val context = LocalContext.current
    val lifecycleOwner = LocalLifecycleOwner.current
    val url = "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4"

    Scaffold(topBar = {
        TopAppBar(
            title = {
                val toolBarText =
                    stringResource(id = R.string.player_header_text)
                Text(
                    text = toolBarText,
                    fontSize = 20.sp,
                    modifier = Modifier
                        .fillMaxWidth()
                )
            },
            contentColor = Color.White
        )
    }) {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            val exoPlayer = remember(context) {
                ExoPlayer.Builder(context).build()
            }

            LaunchedEffect(key1 = Unit) {
                val dataSourceFactory = DefaultDataSourceFactory(
                    context, Util.getUserAgent(context, context.packageName)
                )
                val mediaItem = MediaItem.fromUri(url)
                val source = ProgressiveMediaSource
                    .Factory(dataSourceFactory)
                    .createMediaSource(mediaItem)
                exoPlayer.setMediaSource(source)
                    exoPlayer.prepare()
            }

            AndroidView(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.White),
                factory = {
                    PlayerView(it).apply {
                        player = exoPlayer
                    }
                })

            DisposableEffect(key1 = lifecycleOwner) {
                 val observer = LifecycleEventObserver { _, event ->
                    if (event == Lifecycle.Event.ON_RESUME) {
                        exoPlayer.playWhenReady = true
                    } else if (event == Lifecycle.Event.ON_PAUSE) {
                        exoPlayer.playWhenReady = false
                    }
                }
                lifecycleOwner.lifecycle.addObserver(observer)
                onDispose {
                    exoPlayer.stop()
                    exoPlayer.release()
                    lifecycleOwner.lifecycle.removeObserver(observer)
                }
            }
        }
    }
}